#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="EPICfe"

(test -f $srcdir/configure.in \
## put other tests here
) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level $PKG_NAME directory"
    exit 1
}

echo \*\*\* Killing old .deps
find -name .deps -exec rm -rf {} \; 2>/dev/null

echo \*\*\* Creating aclocal.m4
aclocal
echo \*\*\* Creating Makefiles.in
automake
echo \*\*\* Creating configure
autoconf

echo \*\*\* Configuring / Creating Makefiles
./configure

echo Now type \`make\' to compile $PKG_NAME
