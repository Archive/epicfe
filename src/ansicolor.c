/* EPICfe - A graphical front-end to EPIC
 * Copyright (C) 1998 Nuke Skyjumper
 *                                                                              
 * This program is free software; you can redistribute it and/or modify         
 * it under the terms of the GNU General Public License as published by         
 * the Free Software Foundation; either version 2 of the License, or            
 * (at your option) any later version.                                          
 *                                                                              
 * This program is distributed in the hope that it will be useful,              
 * but WITHOUT ANY WARRANTY; without even the implied warranty of               
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                
 * GNU General Public License for more details.                                 
 *
 * You should have received a copy of the GNU General Public License            
 * along with this program; if not, write to the Free Software                  
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.   
 */

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "colordefs.h"
#include "defines.h"
#include "globals.h"
#include "types.h"

void
ansi_put_char					(GtkText *text,
						 gchar c);

gint	escapelevel;
gchar	*control_code;
GdkColor ansiblack = { 0, 0x0000, 0x0000, 0x0000 };
GdkColor ansiwhite = { 0, 0xffff, 0xffff, 0xffff };
GdkColor ansicolors[8] = {
  { 0, 0x0000, 0x0000, 0x0000 },
  { 0, 0xcd00, 0x0000, 0x0000 },
  { 0, 0x0000, 0xcd00, 0x0000 },
  { 0, 0xcd00, 0xcd00, 0x0000 },
  { 0, 0x0000, 0x0000, 0xcd00 },
  { 0, 0xcd00, 0x0000, 0xcd00 },
  { 0, 0x0000, 0xcd00, 0xcd00 },
  { 0, 0xe500, 0xe500, 0xe500 }
};
GdkColor ansicolors_bold[8] = {
  { 0, 0x4d00, 0x4d00, 0x4d00 },
  { 0, 0xffff, 0x0000, 0x0000 },
  { 0, 0x0000, 0xffff, 0x0000 },
  { 0, 0xffff, 0xffff, 0x0000 },
  { 0, 0x0000, 0x0000, 0xffff },
  { 0, 0xffff, 0x0000, 0xffff },
  { 0, 0x0000, 0xffff, 0xffff },
  { 0, 0xffff, 0xffff, 0xffff }
};

void
ansi_set_colors (void)
{
  static gint	bold;
  gint		color_code;
  GdkColor	*temp_color;

  if (!strlen (control_code))
    {
      strncat (control_code, "0", 1);
    }

/*  while (control_code->len > 0)
    {*/
      color_code = atoi (control_code);
      switch (color_code)
        {
          case 0:
            wdata.color.fore = &ansiwhite;
            wdata.color.back = &ansiblack;
            wdata.font.current = wdata.font.normal;
            bold = 0;
            break;
          case 1: case 5: /* yes, i know 5 isn't bold */
            wdata.font.current = wdata.font.bold;
            bold = TRUE;
            break;
          case 7:
            temp_color = wdata.color.fore;
            wdata.color.fore = wdata.color.back;
            wdata.color.back = temp_color;
            break;
          case 30: case 31: case 32: case 33:
          case 34: case 35: case 36: case 37: 
            if (bold)
              wdata.color.fore = &ansicolors_bold[color_code-30];
            else
              wdata.color.fore = &ansicolors[color_code-30];
            break;
          case 40: case 41: case 42: case 43:
          case 44: case 45: case 46: case 47:
            wdata.color.back = &ansicolors[color_code-40];
            break;
        }
/*    }*/
}

void
ansi_check_code (gchar c)
{ /* ADD ANSI_CODE_SIZE CHECKING HERE */
  gint	i;

  switch (c)
    {
      case '0': case '1': case '2': case '3': case '4':
      case '5': case '6': case '7': case '8': case '9':
        i = strlen (control_code);
	control_code[i] = c;
	control_code[i + 1] = 0;
        break;
      case 'm':
        ansi_set_colors ();
        escapelevel = 0;
        *control_code = (char) 0;
        break;
      case ';':
        ansi_set_colors ();
        *control_code = (char) 0;
        break;
      default:
        escapelevel = 0;
        *control_code = (char) 0;
        break;
    }
}

void
ansi_put_char (GtkText *text, gchar c)
{
  if (escapelevel > 0)
    {
      switch (escapelevel)
        {
          case 1:
            if (c == '[')
              {
                escapelevel++;
                *control_code = (char) 0;
              }
            else
              {
                escapelevel = 0;
              }
            break;
          case 2:
            ansi_check_code (c);
            break;
          default:
            escapelevel = 0;
            *control_code = (char) 0;
            break;
        }
    }
  else
    {
      if (c == 27)
        escapelevel++;
      else
        gtk_text_insert(text, wdata.font.current, wdata.color.fore,
			wdata.color.back, (gchar *) &c, 1);
    }
}
