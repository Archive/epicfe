/* EPICfe - A graphical front-end to EPIC
 * Copyright (C) 1998 Nuke Skyjumper
 *                                                                              
 * This program is free software; you can redistribute it and/or modify         
 * it under the terms of the GNU General Public License as published by         
 * the Free Software Foundation; either version 2 of the License, or            
 * (at your option) any later version.                                          
 *                                                                              
 * This program is distributed in the hope that it will be useful,              
 * but WITHOUT ANY WARRANTY; without even the implied warranty of               
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                
 * GNU General Public License for more details.                                 
 *
 * You should have received a copy of the GNU General Public License            
 * along with this program; if not, write to the Free Software                  
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.   
 */

#ifndef __TYPES_H__
#define __TYPES_H__

typedef struct _WindowFonts	WindowFonts;
typedef struct _WindowColors	WindowColors;
typedef struct _WindowData	WindowData;

struct _WindowFonts
{
  GdkFont	*current;	/* This points to either normal or bold */
  GdkFont	*normal;
  GdkFont	*bold;
};

struct _WindowColors
{
  GdkColor	*fore;
  GdkColor	*back;
};

struct _WindowData
{
  WindowFonts	font;
  WindowColors	color;
};

#endif /* __TYPES_H__ */