/* EPICfe - A graphical front-end to EPIC
 * Copyright (C) 1998 Nuke Skyjumper
 *                                                                              
 * This program is free software; you can redistribute it and/or modify         
 * it under the terms of the GNU General Public License as published by         
 * the Free Software Foundation; either version 2 of the License, or            
 * (at your option) any later version.                                          
 *                                                                              
 * This program is distributed in the hope that it will be useful,              
 * but WITHOUT ANY WARRANTY; without even the implied warranty of               
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                
 * GNU General Public License for more details.                                 
 *
 * You should have received a copy of the GNU General Public License            
 * along with this program; if not, write to the Free Software                  
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.   
 */

#ifndef __DEFINES_H__
#define __DEFINES_H__

/* Maximum number of entries in the command history */
#define MAX_ENTRIES 10

/* Maximum size of an ANSI color code. in theory, they shouldn't ever be more
than about 6 characters, but let's make it fairly large just in case */
#define ANSI_CODE_SIZE 24

/* file descriptors for pipes */
#define PIPE_WRITE_FD 1 
#define PIPE_READ_FD 0 

/* stdout/stdin's file descriptors */
#define STDOUT 1
#define STDIN 0

#endif /* __DEFINES_H__ */
