/* EPICfe - A graphical front-end to EPIC
 * Copyright (C) 1998 Nuke Skyjumper
 *                                                                              
 * This program is free software; you can redistribute it and/or modify         
 * it under the terms of the GNU General Public License as published by         
 * the Free Software Foundation; either version 2 of the License, or            
 * (at your option) any later version.                                          
 *                                                                              
 * This program is distributed in the hope that it will be useful,              
 * but WITHOUT ANY WARRANTY; without even the implied warranty of               
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                
 * GNU General Public License for more details.                                 
 *
 * You should have received a copy of the GNU General Public License            
 * along with this program; if not, write to the Free Software                  
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.   
 */

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "defines.h"
#include "globals.h"
#include "types.h"

#include "textops.h"
#include "ui.h"

int
main (int argc, char *argv[])
{
  pid_t		pid;
  gint		i, oldstdin, oldstdout;
  gint		p1[2], p2[2];
  GtkWidget	*text;

  if (setenv ("TERM", "dumb", 0))
    {
      g_print("Error setting TERM variable\n");
      return 1;
    }


  for (i = 1; i <= argc; i++)
    g_print ("ARG: %d, %s\n", i, argv[i-1]);

  pipe (p1);
  pipe (p2);

  oldstdout = dup (STDOUT);
  oldstdin = dup (STDIN);

  switch (pid = fork ())
    {
      case -1:
	      printf("Unable to fork. aborting\n");
	      return 1;
      case 0: /*        printf("i'm the child, pid %d!\n", getpid());*/
        dup2 (p1[PIPE_WRITE_FD], STDOUT);
        dup2 (p2[PIPE_READ_FD], STDIN);
        close (p1[PIPE_READ_FD]);
        close (p2[PIPE_WRITE_FD]);
        execlp ("irc", "irc", "-d", "Nuke", "127.0.0.1", NULL);
/*        execlp ("cat", "cat", "/etc/sf-interface.irc");*/
        break;
      default: /*       printf("i'm the parent, pid %d!\n", getpid());*/
	gtk_init (&argc, &argv);

	text = ui_create_window ();

	control_code = g_malloc (sizeof (gchar) * ANSI_CODE_SIZE);

        dup2 (p2[PIPE_WRITE_FD], STDOUT);
        close (p1[PIPE_WRITE_FD]);
        close (p2[PIPE_READ_FD]);

	add_input_hooks (p1[PIPE_READ_FD], text);

	gtk_rc_parse ("epicferc");

        gtk_main ();
    }
    
  return 0;
}
