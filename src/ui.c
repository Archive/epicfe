/* EPICfe - A graphical front-end to EPIC
 * Copyright (C) 1998 Nuke Skyjumper
 *                                                                              
 * This program is free software; you can redistribute it and/or modify         
 * it under the terms of the GNU General Public License as published by         
 * the Free Software Foundation; either version 2 of the License, or            
 * (at your option) any later version.                                          
 *                                                                              
 * This program is distributed in the hope that it will be useful,              
 * but WITHOUT ANY WARRANTY; without even the implied warranty of               
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                
 * GNU General Public License for more details.                                 
 *
 * You should have received a copy of the GNU General Public License            
 * along with this program; if not, write to the Free Software                  
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.   
 */

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "colordefs.h"
#include "defines.h"
#include "types.h"

#include "textops.h"

GtkWidget *
ui_create_window					(void);

void
alloc_fonts						(void);

WindowData	wdata;

GtkWidget *
ui_create_window (void)
{
  GtkWidget	*window;
  GtkWidget	*box1;
  GtkWidget	*box2;
  GtkWidget	*box3;
  GtkWidget	*table;
  GtkWidget	*button;
  GtkWidget	*text;
  GtkWidget	*entry;
  GtkWidget	*vscrollbar;

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_usize (window, 650, 200);
  GTK_WINDOW(window)->allow_shrink = TRUE;
  gtk_window_set_title (GTK_WINDOW (window), "EPIC frontend");
  gtk_container_border_width (GTK_CONTAINER (window), 0);
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
                      GTK_SIGNAL_FUNC (gtk_main_quit), NULL);

  alloc_colors (window);
    
  box1 = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (window), box1);

  box2 = gtk_vbox_new (FALSE, 0);
  gtk_container_border_width (GTK_CONTAINER (box2), 0);
  gtk_box_pack_start (GTK_BOX (box1), box2, TRUE, TRUE, 0);

  /* this is the scrolled window to put the text widget inside */
  table = gtk_table_new (2, 2, FALSE);
  gtk_table_set_row_spacing (GTK_TABLE (table), 0, 2);
  gtk_table_set_col_spacing (GTK_TABLE (table), 0, 2);
  gtk_container_border_width (GTK_CONTAINER (table), 0);
  gtk_box_pack_start (GTK_BOX (box2), table, TRUE, TRUE, 0);

  gtk_rc_parse_string ("style \"text\" { base[NORMAL] = {0.0, 0.0, 0.0} }");
  gtk_rc_parse_string ("widget_class \"*GtkText\" style \"text\"");
  gtk_rc_parse_string ("include \"/usr/local/share/gtk/themes/pixmap/gtkrc\"");

  text = gtk_text_new (NULL, NULL);
  gtk_text_set_editable (GTK_TEXT (text), FALSE);
  gtk_text_set_word_wrap (GTK_TEXT (text), TRUE);
  gtk_table_attach (GTK_TABLE (table), text, 0, 1, 0, 1,
                    GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                    GTK_EXPAND | GTK_SHRINK | GTK_FILL, 0, 0);

  vscrollbar = gtk_vscrollbar_new (GTK_TEXT (text)->vadj);

  gtk_table_attach (GTK_TABLE (table), vscrollbar, 1, 2, 0, 1,
                    GTK_FILL, GTK_EXPAND | GTK_SHRINK | GTK_FILL, 0, 0);

/*  separator = gtk_hseparator_new ();
  gtk_box_pack_start (GTK_BOX (box1), separator, FALSE, TRUE, 0);
  gtk_widget_show (separator);*/

  box3 = gtk_vbox_new (FALSE, 0);
  gtk_container_border_width (GTK_CONTAINER (box3), 0);
  gtk_box_pack_start (GTK_BOX (box1), box3, FALSE, TRUE, 0);

  entry = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX (box3), entry, TRUE, TRUE, 0);
  
  gtk_signal_connect (GTK_OBJECT (entry), "key_press_event",
		      (GtkSignalFunc) entry_key_press_callback, (gpointer *) text);
  
  button = gtk_button_new_with_label ("Close");
  gtk_box_pack_start (GTK_BOX (box3), button, TRUE, TRUE, 0);
  gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                             GTK_SIGNAL_FUNC (gtk_widget_destroy),
                             GTK_OBJECT (window));
  
  gtk_widget_grab_focus (entry);
  GTK_WIDGET_UNSET_FLAGS (text, GTK_CAN_FOCUS);
    
  gtk_widget_show_all (window);

  alloc_fonts ();

  return text;
}

void
alloc_fonts (void)
{
  wdata.font.normal = gdk_font_load ("-adobe-helvetica-medium-r-normal-*-12-*-*-*-p-*-iso8859-1");
  wdata.font.bold = gdk_font_load ("-adobe-helvetica-bold-r-normal-*-12-*-*-*-p-*-iso8859-1");
  wdata.font.current = wdata.font.normal;
}