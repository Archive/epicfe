/* EPICfe - A graphical front-end to EPIC
 * Copyright (C) 1998 Nuke Skyjumper
 *                                                                              
 * This program is free software; you can redistribute it and/or modify         
 * it under the terms of the GNU General Public License as published by         
 * the Free Software Foundation; either version 2 of the License, or            
 * (at your option) any later version.                                          
 *                                                                              
 * This program is distributed in the hope that it will be useful,              
 * but WITHOUT ANY WARRANTY; without even the implied warranty of               
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                
 * GNU General Public License for more details.                                 
 *
 * You should have received a copy of the GNU General Public License            
 * along with this program; if not, write to the Free Software                  
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.   
 */

#ifndef __TEXTOPS_H__
#define __TEXTOPS_H__

void
add_input_hooks				(gint		fd,
					 GtkWidget	*text);
void
addtotext	 			(gpointer	   data,
					 gint		   source,
					 GdkInputCondition condition);
void
alloc_colors				(GtkWidget	*window);

void
entry_key_press_callback		(GtkWidget	*widget,
					 GdkEventKey	*event,
					 GtkWidget	*text);

#endif /* __TEXTOPS_H__ */