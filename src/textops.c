/* EPICfe - A graphical front-end to EPIC
 * Copyright (C) 1998 Nuke Skyjumper
 *                                                                              
 * This program is free software; you can redistribute it and/or modify         
 * it under the terms of the GNU General Public License as published by         
 * the Free Software Foundation; either version 2 of the License, or            
 * (at your option) any later version.                                          
 *                                                                              
 * This program is distributed in the hope that it will be useful,              
 * but WITHOUT ANY WARRANTY; without even the implied warranty of               
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                
 * GNU General Public License for more details.                                 
 *
 * You should have received a copy of the GNU General Public License            
 * along with this program; if not, write to the Free Software                  
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.   
 */

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "ansicolor.h"
#include "colordefs.h"
#include "defines.h"
#include "globals.h"
#include "types.h"

void
add_input_hooks				(gint		fd,
					 GtkWidget	*text);
void
addtotext	 			(gpointer	   data,
					 gint		   source,
					 GdkInputCondition condition);
void
alloc_colors				(GtkWidget	*window);

void
entry_key_press_callback		(GtkWidget	*widget,
					 GdkEventKey	*event,
					 GtkWidget	*text);

gint	infile;
gint	irc_fd;

void
add_input_hooks (gint fd, GtkWidget *text)
{
  infile = gdk_input_add (fd, GDK_INPUT_READ, addtotext, text);
  irc_fd = fd;
}

void
addtotext (gpointer data, gint source, GdkInputCondition condition)
{
  gchar   filedata[2048];
  gint    wsize,i;

  wsize = read (irc_fd, filedata, 2047);

  if (wsize == 0) {
    gtk_main_quit ();
    /*close(pstdin);*/
  }
    
  wdata.color.fore = &ansiwhite;
  wdata.color.back = &ansiblack;

  for (i=0; i<wsize; i++)
    ansi_put_char (GTK_TEXT (data), filedata[i]);
}


void
alloc_colors (GtkWidget *window)
{
  gint          i;
  GdkColormap   *colormap;

  colormap = gtk_widget_get_colormap(window);

  for (i=0;i<8;i++)
  {
    gdk_color_alloc(colormap, &ansicolors[i]);
    gdk_color_alloc(colormap, &ansicolors_bold[i]);
  }

  gdk_color_alloc(colormap, &ansiwhite);
  gdk_color_alloc(colormap, &ansiblack);
}

void
entry_key_press_callback (GtkWidget *widget, GdkEventKey *event,
                          GtkWidget *text)
{
  gchar		*entry_text;


  entry_text = gtk_entry_get_text (GTK_ENTRY (widget));

      switch (event->keyval)
        {
          case GDK_Return:
            if (strlen (entry_text))
              {
                g_print ("%s\n", entry_text);
                gtk_entry_set_text (GTK_ENTRY (widget), "");
              }
            break;
          case GDK_Up: case GDK_uparrow:
            gtk_signal_emit_stop_by_name (GTK_OBJECT (widget), "key_press_event");
            
            break;
          case GDK_Down: case GDK_downarrow:
            gtk_signal_emit_stop_by_name (GTK_OBJECT (widget), "key_press_event");

            break;
          case GDK_Page_Up:
            break;
          case GDK_Escape:
            gtk_entry_append_text (GTK_ENTRY (widget), "");
            break;
        }
}
